roots-readme
============

[![npm](https://badge.fury.io/js/roots-readme.png)](http://badge.fury.io/js/roots-readme)  [![dependencies](https://david-dm.org/carrot/roots-readme.png)](https://david-dm.org/carrot/roots-readme)

style your readmes

### License & Contributing

- Details on the license [can be found here](LICENSE.md)
- Details on running tests and contributing [can be found here](contributing.md)

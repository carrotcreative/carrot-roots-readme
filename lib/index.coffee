path = require 'path'
accord = require 'accord'
markdown = accord.load('markdown')

module.exports = ->
  class ReadmeExtension

    fs: ->
      category: 'readme'
      extract: true
      detect: (f) ->
        path.extname(f.relative) == '.md'

    # compile_hooks: ->
    #   category: 'readme'
    #   write: ->
        # false
        # markdown.render(ctx.content)
        #   # .catch(console.error.bind(console))
        #   .done((res) => console.log.bind(res))
